import output

formatting = output.formatChange()

tg = "This Guy I Just Met" # Allows shorterning a long name to short. 

output.write(tg, "Hi!", formatting)
returned = output.question("Me", ["I would like to", "eat", "sleep", "die"], formatting)

output.write( "Me", "I would like to " + returned[1], formatting)

output.write(tg, "That's ok.", formatting)
name = output.stringQuestion(tg, "By the way, what's your name?", formatting)

output.write(tg, "Nice to meet you, " + name, formatting)