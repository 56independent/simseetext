import wrappers as w

tg = "This Guy I Just Met" # Allows shorterning a long name to short. 

w.w(tg, "Hi!")

returned = w.q("", ["I would like to", "eat", "sleep", "die"])

w.w( "Me", "I would like to " + str(returned[1]))

w.w(tg, "That's ok")
name = w.s(tg, "By the way, what's your name?")

w.w(tg, "Nice to meet you, " + name + "!")