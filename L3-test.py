import lists as l
import wrappers as w

tg = "This Guy I Just Met" # Allows shorterning a long name to short. 

to = [
    ["w", tg, "Hi!"]
]

l.parse(to)

returned = w.q("", ["I would like to", "eat", "sleep", "die"])

to = [
    ["w", "Me", "I would like to " + str(returned[1])],
    ["w", tg, "That's ok"]
]

l.parse(to)

name = w.s(tg, "By the way, what's your name?")

to = [
    ["w", tg, "Nice to meet you, " + name + "!"]
]

l.parse(to)