# Introduction
Simseetext is a library which introduces various levels of abstraction to writing purely text-based stories.

# Etymology
The library is:
* Simple
* Easy
* Free
* Plaintext

We can make a 4-way pun being Simseetext. Sim being smple, s being easy, ee being free, and text being plaintext. 


# Tutorial
## Structure
Your folder may be structured how you like, but keep your scripts close together. 

# Level 2
To include subscripts, use `def y():`, and us an indented block. Then do `include x`, with x being the filename. To run the function, do `x.y()`.

In short:

file.py:
```python
def convo():
    ...
```

main.py:

```python
import file

file.convo()
```

This allows files to be split off and make different scenes a lot more readable.

## Level 2
Level 2 provides easy-to-use functions.

# Levels
Each level adds ease to writing stories but removes flexibility.

| Level | Description | Adds | Removes |
|-------|-------------|------|---------|
| 0 | Uses purely code and no abstraction |x|x|
| 1 | Gives functions to writing | Simple-to use functions | Some formatting flexibility |
| 2 | Short functions and easier writing | Shortened functions | Formatting flexibility |
| 3 | List syntax | Simpler writing | Choices |
| 4 | Own Language | A original language | Python scripting |
| 5 | GUI Editor and Viewer | A GUI interface to stories | Code |

## Level 1
Level 1 introduces many functions which make writing easier. Use `import output` and allow yourself to work.

## Level 2
Level 2 introduces wrapper functions. Use `import wrappers` to use. To change formatting, run the function `fc()`.

# Scripting
Simseetext uses Python from its very core to function. This allows powerful scripting capibilites.

To explain python is beyond the scope of this document, however [Python](https://docs.python.org/3/tutorial/index.html) offers a helpful tutorial on writing code. 

At any level below 4, python scripting is possible.
