import textwrap

def checkIfInt(string):
    '''
    Checks if a string can be turned into an interger. If possible, does, if not, dosen't.
    
    Returns results in a list of this format:

    [true, 46]
    
    [false, "forty-six"]
    '''

    if string.isnumeric():
        transfer = True
        integer = int(string)
        return [transfer, integer]
    else:
        transfer = False
        return [transfer, string]

def write(character, text, formatting, pure):
    ''''Writes text to the terminal with formatting specified in the formatting dictionary and character as given.'''

    #if formatting.get("width") != None:
    #    wrapped = textwrap.fill(text, width=formatting["width"])
    #else:
    #    wrapped = text
    #
    #ftext = wrapped

    print(character)
    print("    " + str(text))

    if pure:
        input("") # Breaks text so enter can be used to go at a pace

    print("")

def question( character, text, formatting):
    '''
    Prompts user for a question, with text as a list split in following way:

    ["Do you want to ", "Foo", "Buzz", "Fee"]
    
    With each question in the text in a non-0 index of the list.

    Returns the choice as a list containing both the string and the number of the option in this format:

    [2, "Buzz"]
    '''

    choices = ""
    for i in range(len(text)):
        choices += ("    " + str(i) + " - " + text[i] + "\n")
    
    question = text[0] + "\n" + choices + "\n?"

    write(character, question, formatting, False)

    response = "prevents int checking from resolving true"

    while checkIfInt(response)[0] != True:
        response = input("(int) > ")
    
    responseint = checkIfInt(response)[1]

    print(responseint)
    print(text[responseint])

    return [responseint, str(text[responseint])]

def stringQuestion(character, text, formatting):
    '''
    Takes text in this format:

    "What is your age?"

    and gives back input
    '''

    write(character, text, formatting, False)

    answer = ""

    while answer == "":
        answer = input("> ")

    return answer

def formatChange():
    '''Asks the user a series of questions on setting formatting, and returns resulting dictionary'''
    width = "ojo"
    while checkIfInt(width)[0] != True:
        width = input("What is the text width for long pieces? ")
    widthnum = int(width)
    
    formatting = {
        width: widthnum
    }
    
    return formatting