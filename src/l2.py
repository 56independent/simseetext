import src.l1 as output

global formatting

formatting = {
    "width": 50
}

def fc(formattingFromFunction):
    formatting = formattingFromFunction 

def w(character, text):
    ''''
    Writes text to the terminal, given the name of a character and text to be said.
    '''

    output.write(character, text, formatting, True)

def q(character, text):
    '''
    Like w, but takes text of this format:

    ["Do you want to ", "Foo", "Buzz", "Fee"]

    and returns the numbered index of the option (where foo would be 0, fee 2) and the name in this format:

    [2, "Fee"]
    '''

    return output.question(character, text, formatting)

def s(character, text):
    '''
    Like w, but asks the user for input, and returns what they wrote. 
    '''

    return output.stringQuestion(character, text, formatting)