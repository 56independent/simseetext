import l2 as w

def parse(story):
    '''
    Takes a list:

    to = [
        [character, "text"]
    ]

    and uses the write command to run the dialouge.
    '''

    for i in range(len(story)):
        return exec(story[i][0] + "(\"" + story[i][1] + "\", \"" + story[i][2] + "\")")