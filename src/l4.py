# Level 4 of the program - takes in files and turns them into scripts.

def parse(filename):
    '''
    Takes the name of a simseetext script (without extensions added - assumed to be ".sst"), and converts it to a python script.

    As quoted from a tiddler on the topic:
        The language is simple, but is essentially a thin wrapper around python, so allows full scripting capibilites.
        Functions are called as this, with no need for brackets:
        <function from level 2> <character> <text>
        Comments are started with either a # or // at the beggining of each line, ignoring whitespace.
        The q function and friends return the answer variable, which can be given to another variable. it does not need defining.
        The q function requires its arguments to be given as a list compatible with `l2.o()`
    '''

    import re
    import os

    pyscript = "import src.l2 as l2\n\n# Automatically made code\n\nn = \"\"\n\n" 

    with open(filename + ".sst", "r") as f:
        line = ""

        for l in f: 
            print(l)

            matching = re.match("^([^ ]+) ([^ ]+) ([\"\[].*[\"\]])$", l) # regexr.com/6ii6i
            
            if matching:
                if (matching.group(1) == "q") or (matching.group(1) == "s"): # Adding first thing is different for each program
                    line += "answer = l2." + matching.group(1)
                else:
                    line += "l2." + matching.group(1)

                line += "(" + matching.group(2) + ", " + matching.group(3) + ")" + "\n"
            else:
                line = l
            
            print(line)

            pyscript += line

            print(pyscript)

    with open(filename + ".py", "w") as pyfile:
        pyfile.write(pyscript)

    os.system("python3 " + filename + ".py")

